package com.techu.hackrest.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "repos")

public class RepoModel {

    @Id
    @NotNull

    private String id;
    private String desc;
    private String urlg;

    public RepoModel(){}

    public RepoModel(String id, String desc, String urlg) {
        this.id = id;
        this.desc = desc;
        this.urlg = urlg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUrlg() {
        return urlg;
    }

    public void setUrlg(String urlg) {
        this.urlg = urlg;
    }
}

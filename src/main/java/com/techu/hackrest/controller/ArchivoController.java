package com.techu.hackrest.controller;

import com.techu.hackrest.service.perfilService;
import com.techu.hackrest.service.repoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("hackgit/v1")
public class ArchivoController {

    @Autowired
    repoService repoService;

    @Autowired
    perfilService perfilService;

    @RequestMapping("/archivo/{idPerfil}/{idRepo}")
    public ResponseEntity<String> creaFichero(@PathVariable String idPerfil, @PathVariable String idRepo)
    {
        String cuerpo;
        cuerpo = "#!/bin/bash" + "\n";
        cuerpo = cuerpo + "echo \n";
        cuerpo = cuerpo + "echo iniciando git \n";
        cuerpo = cuerpo + "echo git init \n";
        cuerpo = cuerpo + "git init\n";
        cuerpo = cuerpo + "echo \n";


        cuerpo = cuerpo + "echo definiendo la variable name\n";
        cuerpo = cuerpo + "echo git config --global user.name ";
        cuerpo = cuerpo + perfilService.findById(idPerfil).get().getNombre() + "\n";
        cuerpo = cuerpo + "git config --global user.name ";
        cuerpo = cuerpo + perfilService.findById(idPerfil).get().getNombre() + "\n";
        cuerpo = cuerpo + "echo \n";

        cuerpo = cuerpo + "echo definiendo la variable email\n";
        cuerpo = cuerpo + "echo git config --global user.email ";
        cuerpo = cuerpo + perfilService.findById(idPerfil).get().getCorreo() + "\n";
        cuerpo = cuerpo + "git config --global user.email ";
        cuerpo = cuerpo + perfilService.findById(idPerfil).get().getCorreo() + "\n";
        cuerpo = cuerpo + "echo \n";

        cuerpo = cuerpo + "	PS3=$\"Otro) Salir\n¿Qué deseas hacer en la carpeta actual? \"" + "\n";
        cuerpo = cuerpo + "	options=(\"Descargar el repositorio\" \"Trabajar con el repositorio\")" + "\n";
        cuerpo = cuerpo + "	select opt in \"${options[@]}\"" + "\n";
        cuerpo = cuerpo + "	do" + "\n";
        cuerpo = cuerpo + "		case $opt in" + "\n";
        cuerpo = cuerpo + "			\"Descargar el repositorio\")" + "\n";
        cuerpo = cuerpo + "				echo $opt" + "\n";

        cuerpo = cuerpo + "				git clone ";
        cuerpo = cuerpo + repoService.findById(idRepo).get().getUrlg() + "\n";

        cuerpo = cuerpo + "				echo -------------" + "\n";
        cuerpo = cuerpo + "				;;" + "\n";
        cuerpo = cuerpo + "			\"Trabajar con el repositorio\")" + "\n";
        cuerpo = cuerpo + "				echo $opt" + "\n";

        cuerpo = cuerpo + "				echo conectando al repositorio remoto " + "\n";
        cuerpo = cuerpo + "				echo " + repoService.findById(idRepo).get().getDesc() + "\n";
        cuerpo = cuerpo + "				echo \n";

        cuerpo = cuerpo + "				git remote rm origin" + "\n";
        cuerpo = cuerpo + "				git remote add origin ";
        cuerpo = cuerpo + repoService.findById(idRepo).get().getUrlg() + "\n";
        cuerpo = cuerpo + "				echo \n";


        cuerpo = cuerpo + "				echo -------------" + "\n";
        cuerpo = cuerpo + "				;;" + "\n";
        cuerpo = cuerpo + "			*)" + "\n";
        cuerpo = cuerpo + "				break" + "\n";
        cuerpo = cuerpo + "				;;" + "\n";
        cuerpo = cuerpo + "		esac" + "\n";
        cuerpo = cuerpo + "		echo " + "\n";
        cuerpo = cuerpo + "		break" + "\n";
        cuerpo = cuerpo + "	done" + "\n";

        return (ResponseEntity<String>) ResponseEntity.status(HttpStatus.CREATED)
                .header("Content-Disposition", "attachment; filename=\"hackgit.sh\"")
                .body(cuerpo);
    }

}

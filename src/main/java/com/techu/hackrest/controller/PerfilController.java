package com.techu.hackrest.controller;

import com.techu.hackrest.model.PerfilModel;
import com.techu.hackrest.service.perfilService;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("hackgit/v1")
public class PerfilController {

    @Autowired
    perfilService perfilService;

    @GetMapping("/perfiles")
    public List<PerfilModel> getPerfiles() {
        return perfilService.findAll();
    }

    /* prueba generar fichero
    @RequestMapping("/perfiles2")
    public ResponseEntity<String> consulta()
    {
        //recojo la info de los params reci
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Content-Disposition", "attachment; filename=\"genial.txt\"")
                .body("<h1>kKK</h1>");
    }
    */

    @GetMapping("/perfiles/{id}")
    public Optional<PerfilModel> getPerfilId(@PathVariable String id){
        return perfilService.findById(id);
    }

    @PostMapping("/perfiles")
    public PerfilModel postPerfiles(@RequestBody PerfilModel newPerfil){
        perfilService.save(newPerfil);
        return newPerfil;
    }

    @PutMapping("/perfiles/")
    public boolean putPerfiles(@RequestBody PerfilModel perfilToUpdate){
        if (perfilService.existsByID(perfilToUpdate.getId())){
            perfilService.save(perfilToUpdate);
            return true;
        } else {
                return false;
        }
    }

    @DeleteMapping("/perfiles/")
    public boolean deletePerfiles(@RequestBody PerfilModel perfilToDelete) {
        if (perfilService.existsByID(perfilToDelete.getId())){
            return perfilService.delete(perfilToDelete);
        } else {
            return false;
        }
    }

}

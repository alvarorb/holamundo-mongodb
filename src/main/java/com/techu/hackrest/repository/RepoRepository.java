package com.techu.hackrest.repository;

import com.techu.hackrest.model.RepoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepoRepository extends MongoRepository<RepoModel, String> {



}

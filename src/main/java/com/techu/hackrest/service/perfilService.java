package com.techu.hackrest.service;

import com.techu.hackrest.model.PerfilModel;
import com.techu.hackrest.repository.PerfilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class perfilService {

    @Autowired
    PerfilRepository perfilRepository;

    // Read
    public List<PerfilModel> findAll() {
        return perfilRepository.findAll();
    }

    // Read by Id
    public Optional<PerfilModel> findById(String id){
        return perfilRepository.findById(id);
    }

    // Create
    public PerfilModel save(PerfilModel perfil){
        return perfilRepository.save(perfil);
    }

    // Delete
    public boolean delete(PerfilModel perfil){
        try{
            perfilRepository.delete(perfil);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    // Confirmar si exite
    public boolean existsByID(String id) {
        return(perfilRepository.existsById(id));
    }
}

package com.techu.hackrest.service;

import com.techu.hackrest.model.RepoModel;
import com.techu.hackrest.repository.RepoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class repoService {

    @Autowired
    RepoRepository repoRepository;

    // Read
    public List<RepoModel> findAll() {
        return repoRepository.findAll();
    }

    // Read by Id
    public Optional<RepoModel> findById(String id){
        return repoRepository.findById(id);
    }

    // Create
    public RepoModel save(RepoModel repo){
        return repoRepository.save(repo);
    }

    // Delete
    public boolean delete(RepoModel repo){
        try{
            repoRepository.delete(repo);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    //COnfirmar si existe
    public boolean existsByID(String id) {
        return(repoRepository.existsById(id));
    }
}
